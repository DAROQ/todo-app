module.exports = function(mongoose){
    // This is kind a skeleton, the collection structure will be base on this
    var todoSchema = new mongoose.Schema({
        item: String
    });

    // This is what MongoDB calls 'Collection', in the DB is stored with an 's' for plural 'Todos'
    var Todo = mongoose.model('Todo', todoSchema);
    return Todo;
}