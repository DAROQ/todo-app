var express = require('express');
var todoController = require('./controllers/todoController');


var app = express();

//set up template engine
app.set('view engine', 'ejs');

//Configure the body parser
app.use(express.urlencoded({extended:false}));

//create middleware for static file
app.use('/css', express.static('css'));
app.use('/js', express.static('js'));
app.use('/assets', express.static('assets'));
app.use('/jquery', express.static('node_modules/jquery/dist/'));

//Initialize controllers
todoController(app);

//Port
app.listen(3000);
console.log('Estamos escuchando en el puerto 3000');