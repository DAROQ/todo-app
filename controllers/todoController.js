var mongoose = require('mongoose');

// Connect to the database
mongoose.connect('mongodb://test:test@localhost:27017/toDoApp');

// Import the todo Schema and Model
var todoSchema = require('../schemas/todoSchema');
var Todo = todoSchema(mongoose);

module.exports = function(app){    

    app.get('/todo', function(request, response){
        // Get all the documents in te collection 'Todo' and send them to the view
        Todo.find({}, function(error, data){
            if(error) throw error;
            response.render('todo', {todos: data});                       
        });
        
    });

    app.post('/todo', function(request, response){
        console.log(request.body);
        // Create a new Todo and insert it in the DB.
        var newTodo = Todo(request.body).save(function(error, data){
            if(error) throw error;
            // Send the updated collection back to the client
            response.json(data);
        });         
                       
    });

    app.delete('/todo/:item', function(request, response){
        // Replace '-' for white space
        var todoText = request.params.item.replace(/\-/g, " ");

        // Delete the requested item from the DB.
        Todo.find({ item: todoText }).remove(function(error, data){
            if(error) throw error;
            // Send updated collection back to the client
            response.json(data);                

        });        
    });
}